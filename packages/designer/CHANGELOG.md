# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.8.163](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.162...@vtj/designer@0.8.163) (2024-10-17)


### Bug Fixes

* 🐛 element-plus 2.8.5 bug 自定义校验函数导致ElSelect异常 ([79c2d71](https://gitee.com/newgateway/vtj/commits/79c2d71b3363afd4445a3a6f6738f1c998509a3a))





## [0.8.162](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.161...@vtj/designer@0.8.162) (2024-10-15)

**Note:** Version bump only for package @vtj/designer





## [0.8.161](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.160...@vtj/designer@0.8.161) (2024-10-14)

**Note:** Version bump only for package @vtj/designer






## [0.8.160](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.159...@vtj/designer@0.8.160) (2024-10-14)


### Bug Fixes

* 🐛 parseExpression error ([3c3f555](https://gitee.com/newgateway/vtj/commits/3c3f5559153ad8eba11ae688c84133421f2e9021))






## [0.8.159](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.158...@vtj/designer@0.8.159) (2024-10-11)

**Note:** Version bump only for package @vtj/designer






## [0.8.158](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.157...@vtj/designer@0.8.158) (2024-10-11)


### Features

* ✨ 区块管理支持分组 ([08f64a0](https://gitee.com/newgateway/vtj/commits/08f64a09088ef42142fc607ba8099afe6946c35f))
* ✨ api分组 ([bd029b0](https://gitee.com/newgateway/vtj/commits/bd029b0418ef48afac4830ed158e00c1771a55e0))





## [0.8.157](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.156...@vtj/designer@0.8.157) (2024-10-09)


### Bug Fixes

* 🐛 只有一个名为default的插槽，并且无参数，可以省略 ([3a16e2d](https://gitee.com/newgateway/vtj/commits/3a16e2dbdb776b44dbb6ca24c1ac57170503d5b7))





## [0.8.156](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.155...@vtj/designer@0.8.156) (2024-10-08)


### Bug Fixes

* 🐛 XDialog classList bug ([d96b8ba](https://gitee.com/newgateway/vtj/commits/d96b8ba4901f69249707a955638a40902d79bd73))






## [0.8.155](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.154...@vtj/designer@0.8.155) (2024-10-07)

**Note:** Version bump only for package @vtj/designer






## [0.8.154](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.153...@vtj/designer@0.8.154) (2024-10-07)

**Note:** Version bump only for package @vtj/designer





## [0.8.153](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.152...@vtj/designer@0.8.153) (2024-10-07)

**Note:** Version bump only for package @vtj/designer





## [0.8.152](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.151...@vtj/designer@0.8.152) (2024-10-04)

**Note:** Version bump only for package @vtj/designer





## [0.8.151](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.150...@vtj/designer@0.8.151) (2024-10-04)


### Bug Fixes

* 🐛 页面管理数量统计 ([0cdc62b](https://gitee.com/newgateway/vtj/commits/0cdc62bc2ab7a503b15b0c9a7745542a0dfe0fe7))






## [0.8.150](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.149...@vtj/designer@0.8.150) (2024-09-30)

**Note:** Version bump only for package @vtj/designer





## [0.8.149](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.148...@vtj/designer@0.8.149) (2024-09-27)

**Note:** Version bump only for package @vtj/designer





## [0.8.148](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.147...@vtj/designer@0.8.148) (2024-09-27)

**Note:** Version bump only for package @vtj/designer






## [0.8.147](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.146...@vtj/designer@0.8.147) (2024-09-25)


### Features

* ✨ 初始化vant物料 ([9279d61](https://gitee.com/newgateway/vtj/commits/9279d61ebbf90a2824a157ad2d182294627a3bad))






## [0.8.146](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.145...@vtj/designer@0.8.146) (2024-09-20)


### Bug Fixes

* 🐛 画布拖拽结束状态没更新 ([e5209d3](https://gitee.com/newgateway/vtj/commits/e5209d3fd010c7275f731816e0078a12a807fe9a))






## [0.8.145](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.144...@vtj/designer@0.8.145) (2024-09-20)


### Features

* ✨ 画布支持直接拖拽节点 ([5dde38d](https://gitee.com/newgateway/vtj/commits/5dde38d401aaa2e040e97e320fb09b67a3ed1b48))






## [0.8.144](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.143...@vtj/designer@0.8.144) (2024-09-19)

**Note:** Version bump only for package @vtj/designer





## [0.8.143](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.142...@vtj/designer@0.8.143) (2024-09-19)

**Note:** Version bump only for package @vtj/designer





## [0.8.142](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.141...@vtj/designer@0.8.142) (2024-09-18)


### Bug Fixes

* 🐛 设计器预览样式丢失问题 ([fb07321](https://gitee.com/newgateway/vtj/commits/fb0732140d4e095daf52b1cfa12a72e1ab65c178))





## [0.8.141](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.140...@vtj/designer@0.8.141) (2024-09-18)


### Features

* ✨ 打包移除vxe-table ([27e9fd5](https://gitee.com/newgateway/vtj/commits/27e9fd501d76d2890e656f9676e07150bbd1f72f))






## [0.8.140](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.139...@vtj/designer@0.8.140) (2024-09-18)


### Bug Fixes

* 🐛 dev-tools ([07008d9](https://gitee.com/newgateway/vtj/commits/07008d98d1778081ef0c9ed74f659babf0476fc6))





## [0.8.139](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.138...@vtj/designer@0.8.139) (2024-09-16)

**Note:** Version bump only for package @vtj/designer





## [0.8.138](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.137...@vtj/designer@0.8.138) (2024-09-16)

**Note:** Version bump only for package @vtj/designer





## [0.8.137](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.136...@vtj/designer@0.8.137) (2024-09-16)


### Bug Fixes

* 🐛 升级vue3.5 ([3b12449](https://gitee.com/newgateway/vtj/commits/3b12449447692487882539d43210d57dcc97a48a))
* 🐛 devtools ([7ec8411](https://gitee.com/newgateway/vtj/commits/7ec8411956b43a9eadd022c253f382d13233f596))






## [0.8.136](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.135...@vtj/designer@0.8.136) (2024-09-14)


### Bug Fixes

* 🐛 设计器样式污染预览页面样式 ([f3631cd](https://gitee.com/newgateway/vtj/commits/f3631cdb440c6f1d5a327763a4d4695835cf3782))






## [0.8.135](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.134...@vtj/designer@0.8.135) (2024-09-13)

**Note:** Version bump only for package @vtj/designer





## [0.8.134](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.133...@vtj/designer@0.8.134) (2024-09-12)

**Note:** Version bump only for package @vtj/designer






## [0.8.133](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.132...@vtj/designer@0.8.133) (2024-09-12)

**Note:** Version bump only for package @vtj/designer





## [0.8.132](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.131...@vtj/designer@0.8.132) (2024-09-12)


### Bug Fixes

* 🐛 devtools delay load ([2c20df7](https://gitee.com/newgateway/vtj/commits/2c20df765cb28ac14c1fd1c9b4e8aa773e9628bd))





## [0.8.131](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.130...@vtj/designer@0.8.131) (2024-09-12)


### Features

* ✨ devtools ([1afcfcb](https://gitee.com/newgateway/vtj/commits/1afcfcb1389dd65c3c94dda3ee7d0472424e9067))
* ✨ devtools module ([18e2949](https://gitee.com/newgateway/vtj/commits/18e294909e0533119f2d6f7d9fa33d470f3a6abb))






## [0.8.130](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.129...@vtj/designer@0.8.130) (2024-09-10)

**Note:** Version bump only for package @vtj/designer





## [0.8.129](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.128...@vtj/designer@0.8.129) (2024-09-10)

**Note:** Version bump only for package @vtj/designer






## [0.8.128](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.127...@vtj/designer@0.8.128) (2024-09-09)

**Note:** Version bump only for package @vtj/designer






## [0.8.127](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.126...@vtj/designer@0.8.127) (2024-09-09)

**Note:** Version bump only for package @vtj/designer






## [0.8.126](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.125...@vtj/designer@0.8.126) (2024-09-09)

**Note:** Version bump only for package @vtj/designer






## [0.8.125](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.124...@vtj/designer@0.8.125) (2024-09-08)


### Bug Fixes

* 🐛 更新依赖导致的兼容问题 ([d96fe5d](https://gitee.com/newgateway/vtj/commits/d96fe5d457ab1dc35ace670dca062a3cd86894c2))





## [0.8.124](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.123...@vtj/designer@0.8.124) (2024-09-06)


### Bug Fixes

* 🐛 plugin is undefined error ([82f370c](https://gitee.com/newgateway/vtj/commits/82f370cc3d8d1db757cffcd843454aa1b4e2653c))






## [0.8.123](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.122...@vtj/designer@0.8.123) (2024-09-02)

**Note:** Version bump only for package @vtj/designer





## [0.8.122](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.121...@vtj/designer@0.8.122) (2024-09-02)


### Bug Fixes

* 🐛 增加库依赖失效 ([441f71c](https://gitee.com/newgateway/vtj/commits/441f71cfbc6afcce41c83b00c881fc20ea287b42))






## [0.8.121](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.120...@vtj/designer@0.8.121) (2024-09-02)


### Bug Fixes

* 🐛 文件管理上传文件后url不刷新，调整pad视图尺寸 ([e9de789](https://gitee.com/newgateway/vtj/commits/e9de7894213108e4c2b84f9a8614e1ad62f247eb))


### Features

* ✨ 删除页面同时删除源码文件 ([7a67f51](https://gitee.com/newgateway/vtj/commits/7a67f51983118c8f982b6c18d1464c602d39943f))






## [0.8.120](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.119...@vtj/designer@0.8.120) (2024-08-31)


### Features

* ✨ 页面路由复制 ([0da2890](https://gitee.com/newgateway/vtj/commits/0da289059c7330ef67e49b329044bd828217edb0))






## [0.8.119](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.118...@vtj/designer@0.8.119) (2024-08-29)

**Note:** Version bump only for package @vtj/designer





## [0.8.118](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.117...@vtj/designer@0.8.118) (2024-08-29)

**Note:** Version bump only for package @vtj/designer






## [0.8.117](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.116...@vtj/designer@0.8.117) (2024-08-29)

**Note:** Version bump only for package @vtj/designer





## [0.8.116](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.115...@vtj/designer@0.8.116) (2024-08-28)


### Bug Fixes

* 🐛 setter labelWidth ([4b970f6](https://gitee.com/newgateway/vtj/commits/4b970f671577fe32ff60b80d2a65b67d51cae88b))





## [0.8.115](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.114...@vtj/designer@0.8.115) (2024-08-28)

**Note:** Version bump only for package @vtj/designer






## [0.8.114](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.113...@vtj/designer@0.8.114) (2024-08-27)

**Note:** Version bump only for package @vtj/designer






## [0.8.113](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.112...@vtj/designer@0.8.113) (2024-08-23)


### Features

* ✨ 自动判断label宽度 ([6b28fd4](https://gitee.com/newgateway/vtj/commits/6b28fd46f751da3d4d0f70d3efa457a3bd25a3cb))





## [0.8.112](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.111...@vtj/designer@0.8.112) (2024-08-22)

**Note:** Version bump only for package @vtj/designer





## [0.8.111](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.110...@vtj/designer@0.8.111) (2024-08-22)

**Note:** Version bump only for package @vtj/designer





## [0.8.110](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.109...@vtj/designer@0.8.110) (2024-08-22)

**Note:** Version bump only for package @vtj/designer






## [0.8.109](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.108...@vtj/designer@0.8.109) (2024-08-20)

**Note:** Version bump only for package @vtj/designer






## [0.8.108](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.107...@vtj/designer@0.8.108) (2024-08-18)


### Features

* ✨ 优化属性设置面板 ([d427a0f](https://gitee.com/newgateway/vtj/commits/d427a0fa3f848547d295256440847e394221f312))





## [0.8.107](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.106...@vtj/designer@0.8.107) (2024-08-17)

**Note:** Version bump only for package @vtj/designer





## [0.8.106](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.105...@vtj/designer@0.8.106) (2024-08-17)


### Bug Fixes

* 🐛 不校验上下文对象 ([ed79d7d](https://gitee.com/newgateway/vtj/commits/ed79d7d70ecff6070fa9e2a9d12b1818fe16d8e9))





## [0.8.105](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.104...@vtj/designer@0.8.105) (2024-08-16)

**Note:** Version bump only for package @vtj/designer





## [0.8.104](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.103...@vtj/designer@0.8.104) (2024-08-16)

**Note:** Version bump only for package @vtj/designer






## [0.8.103](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.102...@vtj/designer@0.8.103) (2024-08-14)

**Note:** Version bump only for package @vtj/designer





## [0.8.102](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.101...@vtj/designer@0.8.102) (2024-08-13)

**Note:** Version bump only for package @vtj/designer





## [0.8.101](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.100...@vtj/designer@0.8.101) (2024-08-13)

**Note:** Version bump only for package @vtj/designer






## [0.8.100](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.99...@vtj/designer@0.8.100) (2024-08-09)


### Bug Fixes

* 🐛 style设置border回填不正确 ([d05da3c](https://gitee.com/newgateway/vtj/commits/d05da3c28d6ec611a99b5417ba2db23bca72555d))





## [0.8.99](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.98...@vtj/designer@0.8.99) (2024-08-08)


### Bug Fixes

* 🐛 插槽构造错误 ([da1bafe](https://gitee.com/newgateway/vtj/commits/da1bafe2a0c6e2761dc954ac6647f22f2b13ef0e))





## [0.8.98](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.97...@vtj/designer@0.8.98) (2024-08-08)


### Bug Fixes

* 🐛 绑定上下文代码校验问题 ([01b3aca](https://gitee.com/newgateway/vtj/commits/01b3acaa69fe8ef8af30587ceaf6481a91a3221d))






## [0.8.97](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.96...@vtj/designer@0.8.97) (2024-07-31)

**Note:** Version bump only for package @vtj/designer






## [0.8.96](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.95...@vtj/designer@0.8.96) (2024-07-25)

**Note:** Version bump only for package @vtj/designer





## [0.8.95](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.94...@vtj/designer@0.8.95) (2024-07-22)

**Note:** Version bump only for package @vtj/designer





## [0.8.94](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.93...@vtj/designer@0.8.94) (2024-07-20)


### Bug Fixes

* 🐛 绑定器支持最大化 ([7c852f7](https://gitee.com/newgateway/vtj/commits/7c852f7514629d247ea498d0ac3170871dd6b551))





## [0.8.93](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.92...@vtj/designer@0.8.93) (2024-07-20)

**Note:** Version bump only for package @vtj/designer





## [0.8.92](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.91...@vtj/designer@0.8.92) (2024-07-19)

**Note:** Version bump only for package @vtj/designer





## [0.8.91](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.90...@vtj/designer@0.8.91) (2024-07-19)


### Bug Fixes

* 🐛 双向绑定属性提取错误 ([7482192](https://gitee.com/newgateway/vtj/commits/74821920edfc8495b2f4f388c07630f9906a35cd))





## [0.8.90](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.89...@vtj/designer@0.8.90) (2024-07-18)

**Note:** Version bump only for package @vtj/designer





## [0.8.89](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.88...@vtj/designer@0.8.89) (2024-07-18)

**Note:** Version bump only for package @vtj/designer





## [0.8.88](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.87...@vtj/designer@0.8.88) (2024-07-16)

**Note:** Version bump only for package @vtj/designer





## [0.8.87](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.86...@vtj/designer@0.8.87) (2024-07-16)

**Note:** Version bump only for package @vtj/designer





## [0.8.86](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.85...@vtj/designer@0.8.86) (2024-07-15)

**Note:** Version bump only for package @vtj/designer





## [0.8.85](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.84...@vtj/designer@0.8.85) (2024-07-15)

**Note:** Version bump only for package @vtj/designer





## [0.8.84](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.83...@vtj/designer@0.8.84) (2024-07-15)

**Note:** Version bump only for package @vtj/designer





## [0.8.83](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.82...@vtj/designer@0.8.83) (2024-07-12)

**Note:** Version bump only for package @vtj/designer





## [0.8.82](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.81...@vtj/designer@0.8.82) (2024-07-12)


### Bug Fixes

* 🐛 getVueInstance 异步组件失效 ([3774776](https://gitee.com/newgateway/vtj/commits/377477630d83a9e2b97aee95682e35bb6062ee4c))





## [0.8.81](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.80...@vtj/designer@0.8.81) (2024-07-12)

**Note:** Version bump only for package @vtj/designer





## [0.8.80](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.79...@vtj/designer@0.8.80) (2024-07-10)

**Note:** Version bump only for package @vtj/designer





## [0.8.79](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.78...@vtj/designer@0.8.79) (2024-07-10)


### Features

* ✨ 设计器支持动态插槽 ([c9f8d7e](https://gitee.com/newgateway/vtj/commits/c9f8d7e4016b6e410ff878a75f36513c8db45a19))





## [0.8.78](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.77...@vtj/designer@0.8.78) (2024-07-09)

**Note:** Version bump only for package @vtj/designer





## [0.8.77](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.76...@vtj/designer@0.8.77) (2024-07-09)


### Bug Fixes

* 🐛 接触绑定变量值改为undefined ([e49b4b4](https://gitee.com/newgateway/vtj/commits/e49b4b40f99e87f24bdc84ff96dc0cff36f84f8d))





## [0.8.76](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.75...@vtj/designer@0.8.76) (2024-07-08)

**Note:** Version bump only for package @vtj/designer





## [0.8.75](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.74...@vtj/designer@0.8.75) (2024-07-08)

**Note:** Version bump only for package @vtj/designer





## [0.8.74](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.73...@vtj/designer@0.8.74) (2024-07-06)

**Note:** Version bump only for package @vtj/designer





## [0.8.73](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.72...@vtj/designer@0.8.73) (2024-07-06)

**Note:** Version bump only for package @vtj/designer





## [0.8.72](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.71...@vtj/designer@0.8.72) (2024-07-06)

**Note:** Version bump only for package @vtj/designer





## [0.8.71](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.70...@vtj/designer@0.8.71) (2024-07-05)


### Performance Improvements

* ⚡ 优化错误提示 ([d965bda](https://gitee.com/newgateway/vtj/commits/d965bda468cda1d561a9f53c3a8e2f78ae24c4ad))





## [0.8.70](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.69...@vtj/designer@0.8.70) (2024-07-05)

**Note:** Version bump only for package @vtj/designer





## [0.8.69](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.68...@vtj/designer@0.8.69) (2024-07-04)

**Note:** Version bump only for package @vtj/designer





## [0.8.68](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.67...@vtj/designer@0.8.68) (2024-07-04)

**Note:** Version bump only for package @vtj/designer





## [0.8.67](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.66...@vtj/designer@0.8.67) (2024-07-03)

**Note:** Version bump only for package @vtj/designer





## [0.8.66](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.65...@vtj/designer@0.8.66) (2024-07-03)

**Note:** Version bump only for package @vtj/designer





## [0.8.65](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.64...@vtj/designer@0.8.65) (2024-07-02)

**Note:** Version bump only for package @vtj/designer





## [0.8.64](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.63...@vtj/designer@0.8.64) (2024-06-28)

**Note:** Version bump only for package @vtj/designer





## [0.8.63](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.62...@vtj/designer@0.8.63) (2024-06-28)


### Bug Fixes

* 🐛 修复设计器不能选中异步组件 ([f09c73e](https://gitee.com/newgateway/vtj/commits/f09c73e9b73e7577b98f8604537720d8f0d56122))





## [0.8.62](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.61...@vtj/designer@0.8.62) (2024-06-27)

**Note:** Version bump only for package @vtj/designer





## [0.8.61](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.60...@vtj/designer@0.8.61) (2024-06-26)

**Note:** Version bump only for package @vtj/designer





## [0.8.60](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.59...@vtj/designer@0.8.60) (2024-06-26)

**Note:** Version bump only for package @vtj/designer





## [0.8.59](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.58...@vtj/designer@0.8.59) (2024-06-26)

**Note:** Version bump only for package @vtj/designer





## [0.8.58](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.57...@vtj/designer@0.8.58) (2024-06-26)

**Note:** Version bump only for package @vtj/designer





## [0.8.57](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.56...@vtj/designer@0.8.57) (2024-06-25)

**Note:** Version bump only for package @vtj/designer





## [0.8.56](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.55...@vtj/designer@0.8.56) (2024-06-22)


### Features

* ✨ api管理支持搜索，帮助中心支持返回首页 ([cbc1e9c](https://gitee.com/newgateway/vtj/commits/cbc1e9c0eb2fe15e52b98491178c2ba5ed56f423))





## [0.8.55](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.54...@vtj/designer@0.8.55) (2024-06-21)

**Note:** Version bump only for package @vtj/designer





## [0.8.54](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.53...@vtj/designer@0.8.54) (2024-06-20)

**Note:** Version bump only for package @vtj/designer





## [0.8.53](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.52...@vtj/designer@0.8.53) (2024-06-20)

**Note:** Version bump only for package @vtj/designer





## [0.8.52](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.51...@vtj/designer@0.8.52) (2024-06-18)

**Note:** Version bump only for package @vtj/designer





## [0.8.51](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.50...@vtj/designer@0.8.51) (2024-06-16)

**Note:** Version bump only for package @vtj/designer





## [0.8.50](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.49...@vtj/designer@0.8.50) (2024-06-14)

**Note:** Version bump only for package @vtj/designer





## [0.8.49](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.48...@vtj/designer@0.8.49) (2024-06-14)


### Features

* ✨ 记录环境，在扩展时可能需要用到 ([18419e7](https://gitee.com/newgateway/vtj/commits/18419e74973dffe6010a2ac4c4b1bd04df5ec0b7))





## [0.8.48](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.47...@vtj/designer@0.8.48) (2024-06-14)

**Note:** Version bump only for package @vtj/designer





## [0.8.47](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.46...@vtj/designer@0.8.47) (2024-06-14)

**Note:** Version bump only for package @vtj/designer





## [0.8.46](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.45...@vtj/designer@0.8.46) (2024-06-14)

**Note:** Version bump only for package @vtj/designer





## [0.8.45](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.44...@vtj/designer@0.8.45) (2024-06-14)

**Note:** Version bump only for package @vtj/designer





## [0.8.44](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.43...@vtj/designer@0.8.44) (2024-06-13)

**Note:** Version bump only for package @vtj/designer





## [0.8.43](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.42...@vtj/designer@0.8.43) (2024-06-12)

**Note:** Version bump only for package @vtj/designer





## [0.8.42](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.41...@vtj/designer@0.8.42) (2024-06-12)

**Note:** Version bump only for package @vtj/designer





## [0.8.41](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.40...@vtj/designer@0.8.41) (2024-06-12)

**Note:** Version bump only for package @vtj/designer





## [0.8.40](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.39...@vtj/designer@0.8.40) (2024-06-06)


### Bug Fixes

* 🐛 修复类型、style透明度回填错误 ([eb92ad6](https://gitee.com/newgateway/vtj/commits/eb92ad66cff7e80197f09e91c21eff7dac2ea14f))





## [0.8.39](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.38...@vtj/designer@0.8.39) (2024-05-30)


### Features

* ✨ 事件插槽定义支持配置参数 ([d6728b3](https://gitee.com/newgateway/vtj/commits/d6728b330815a937cb582f0d0afb667f54bc590e))





## [0.8.38](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.37...@vtj/designer@0.8.38) (2024-05-29)

**Note:** Version bump only for package @vtj/designer





## [0.8.37](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.36...@vtj/designer@0.8.37) (2024-05-28)

**Note:** Version bump only for package @vtj/designer





## [0.8.36](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.35...@vtj/designer@0.8.36) (2024-05-27)

**Note:** Version bump only for package @vtj/designer





## [0.8.35](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.34...@vtj/designer@0.8.35) (2024-05-27)

**Note:** Version bump only for package @vtj/designer





## [0.8.34](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.33...@vtj/designer@0.8.34) (2024-05-24)

**Note:** Version bump only for package @vtj/designer





## [0.8.33](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.32...@vtj/designer@0.8.33) (2024-05-23)

**Note:** Version bump only for package @vtj/designer





## [0.8.32](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.30...@vtj/designer@0.8.32) (2024-05-23)


### Bug Fixes

* 🐛 provider install ([587112d](https://gitee.com/newgateway/vtj/commits/587112d873cb5738691be63b269d16e04ae9312e))


### Features

* ✨ ui物料 ([3429074](https://gitee.com/newgateway/vtj/commits/34290740f2a2f125c033b7e3cf3bcbea4e48c1bc))





## [0.8.31](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.30...@vtj/designer@0.8.31) (2024-05-14)

**Note:** Version bump only for package @vtj/designer





## [0.8.30](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.29...@vtj/designer@0.8.30) (2024-05-11)


### Features

* ✨ 变量绑定高级列表支持方法和数据类型展示 ([029d70b](https://gitee.com/newgateway/vtj/commits/029d70bbf5a7cb0e3fc376ee7b1283f4d4517a26))
* ✨ 变量绑定高级列表支持方法和数据类型展示 ([2d8bd17](https://gitee.com/newgateway/vtj/commits/2d8bd175226f6dee0bfa9f319d9c16ec876d265f))





## [0.8.29](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.28...@vtj/designer@0.8.29) (2024-05-11)


### Bug Fixes

* 🐛 provider install ([92f7535](https://gitee.com/newgateway/vtj/commits/92f75352286ec4956ce0b8b6cee752fab6730216))





## [0.8.28](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.27...@vtj/designer@0.8.28) (2024-05-09)


### Bug Fixes

* 🐛 更改帮助中心链接 ([e59ba08](https://gitee.com/newgateway/vtj/commits/e59ba0890a419a7b11bfd63e2120c2486abfcc3b))





## [0.8.27](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.26...@vtj/designer@0.8.27) (2024-05-08)


### Features

* ✨ 支持metaQuery ([94c2879](https://gitee.com/newgateway/vtj/commits/94c287930d3ae9bafab7419673b4ec5c4fc5c73a))





## [0.8.26](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.25...@vtj/designer@0.8.26) (2024-05-07)

**Note:** Version bump only for package @vtj/designer





## [0.8.25](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.24...@vtj/designer@0.8.25) (2024-05-07)

**Note:** Version bump only for package @vtj/designer





## [0.8.24](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.23...@vtj/designer@0.8.24) (2024-05-07)

**Note:** Version bump only for package @vtj/designer





## [0.8.23](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.22...@vtj/designer@0.8.23) (2024-05-07)

**Note:** Version bump only for package @vtj/designer





## [0.8.22](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.21...@vtj/designer@0.8.22) (2024-05-07)

**Note:** Version bump only for package @vtj/designer





## [0.8.21](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.20...@vtj/designer@0.8.21) (2024-05-06)

**Note:** Version bump only for package @vtj/designer





## [0.8.20](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.19...@vtj/designer@0.8.20) (2024-05-06)

**Note:** Version bump only for package @vtj/designer





## [0.8.19](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.18...@vtj/designer@0.8.19) (2024-05-06)

**Note:** Version bump only for package @vtj/designer





## [0.8.18](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.17...@vtj/designer@0.8.18) (2024-05-04)

**Note:** Version bump only for package @vtj/designer





## [0.8.17](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.16...@vtj/designer@0.8.17) (2024-05-02)


### Features

* ✨ extension add params ([ce517b8](https://gitee.com/newgateway/vtj/commits/ce517b8075a491186fc69ae8a757d810022538d4))





## [0.8.16](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.15...@vtj/designer@0.8.16) (2024-05-02)

**Note:** Version bump only for package @vtj/designer





## [0.8.15](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.14...@vtj/designer@0.8.15) (2024-05-02)


### Bug Fixes

* 🐛 engine 处理 adapter ([03478a9](https://gitee.com/newgateway/vtj/commits/03478a9ce700dddcaccc5f5922f4e3391ad001b2))





## [0.8.14](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.13...@vtj/designer@0.8.14) (2024-05-02)

**Note:** Version bump only for package @vtj/designer





## [0.8.13](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.12...@vtj/designer@0.8.13) (2024-05-02)


### Features

* ✨ 插件增加install参数 ([45b704b](https://gitee.com/newgateway/vtj/commits/45b704b445bf7f7c935844a2ee53b5714f4f2140))





## [0.8.12](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.11...@vtj/designer@0.8.12) (2024-05-01)

**Note:** Version bump only for package @vtj/designer





## [0.8.11](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.10...@vtj/designer@0.8.11) (2024-05-01)

**Note:** Version bump only for package @vtj/designer





## [0.8.10](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.9...@vtj/designer@0.8.10) (2024-05-01)

**Note:** Version bump only for package @vtj/designer





## [0.8.9](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.8...@vtj/designer@0.8.9) (2024-04-30)


### Features

* ✨ 设计器支持远程扩展 ([ed2ed8e](https://gitee.com/newgateway/vtj/commits/ed2ed8ec38f51d389d0eb05488a3e8e06a1fdd35))






## [0.8.8](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.7...@vtj/designer@0.8.8) (2024-04-27)


### Bug Fixes

* 🐛 deps ([f88b4f7](https://gitee.com/newgateway/vtj/commits/f88b4f72a634e48fa516e2b1e8e0c87d3a968ef2))





## [0.8.7](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.5...@vtj/designer@0.8.7) (2024-04-26)


### Features

* ✨ add charts module ([2e5b7e9](https://gitee.com/newgateway/vtj/commits/2e5b7e9ca763a2446d3e65af6fa8d1d32b8f2243))





## [0.8.6](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.5...@vtj/designer@0.8.6) (2024-04-26)


### Features

* ✨ add charts module ([2e5b7e9](https://gitee.com/newgateway/vtj/commits/2e5b7e9ca763a2446d3e65af6fa8d1d32b8f2243))






## [0.8.5](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.4...@vtj/designer@0.8.5) (2024-04-24)


### Bug Fixes

* 🐛 更新项目信息不能正常渲染插件 ([a8ddf43](https://gitee.com/newgateway/vtj/commits/a8ddf4356dc03e1b430ebeba4195c1af88c20827))
* 🐛 加载插件css文件环境错误 ([43dface](https://gitee.com/newgateway/vtj/commits/43dface3f7d0c5c3ed78a8586567f06392be7040))





## [0.8.4](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.3...@vtj/designer@0.8.4) (2024-04-24)


### Bug Fixes

* 🐛 设计画布拖拽组件需要判断，不能有无限递归 ([6f0d412](https://gitee.com/newgateway/vtj/commits/6f0d412fe45cfc9428ea88c6b84cf4074177ac5c))






## [0.8.3](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.2...@vtj/designer@0.8.3) (2024-04-23)


### Bug Fixes

* 🐛 大纲树中拖拽子节点放置到根节点，dsl没有更新 ([c6a020b](https://gitee.com/newgateway/vtj/commits/c6a020b8b5f5fdec9b5e58a87fb750589f2d5668))
* 🐛 历史记录需要回写block dsl ([5002912](https://gitee.com/newgateway/vtj/commits/500291222323f57ee5846a382a63821c51a7d8b1))


### Features

* ✨ 设计画布支持拖拽 ([a4e8866](https://gitee.com/newgateway/vtj/commits/a4e8866a4e0bf118fcfca37cd98aa81bb1029059))






## [0.8.2](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.1...@vtj/designer@0.8.2) (2024-04-22)


### Bug Fixes

* 🐛 编译后不应该调service.init; 出码$props 改为 props ([b3ab003](https://gitee.com/newgateway/vtj/commits/b3ab003c59df81225da8b0a43593f2b28f7bf53b))
* 🐛 出码作用域插槽没传递出来 ([bfdcf63](https://gitee.com/newgateway/vtj/commits/bfdcf630d5a67d4e7d6e60cb23537da9a89ae224))
* 🐛 定义事件的事件名称不支持 update:modelValue 格式，无法增加双向绑定 ([3cba3c7](https://gitee.com/newgateway/vtj/commits/3cba3c76687f2e3a0842ae6957f69266c0fd92fe))





## [0.8.1](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.8.0...@vtj/designer@0.8.1) (2024-04-22)


### Bug Fixes

* 🐛 cli template ([911c3a0](https://gitee.com/newgateway/vtj/commits/911c3a0e2bb60548affe5dcf5a496577809d63b8))






# [0.8.0](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.7.34...@vtj/designer@0.8.0) (2024-04-22)


### Bug Fixes

* 🐛 修复组件渲染根节点是text类型时无法选中设置属性 ([4bce89e](https://gitee.com/newgateway/vtj/commits/4bce89e21608d1eabeb89de4f451ee2abb556feb))


### Features

* ✨ 依赖支持语言包设置 ([362190e](https://gitee.com/newgateway/vtj/commits/362190e66d663412d2d07261bc29b9ef439af8ed))
* ✨ 支持 UrlSchema ([edae2c5](https://gitee.com/newgateway/vtj/commits/edae2c52ce88ad72a4a7c31844385dc083249e72))
* ✨ 支持区块插件 ([88b5028](https://gitee.com/newgateway/vtj/commits/88b5028cdb142dd9f5642c51ecb9f978333858ce))
* ✨ add FileSetter ([8467349](https://gitee.com/newgateway/vtj/commits/8467349162e8694808fd3fdcd2354d8bb6de086f))


### Performance Improvements

* ⚡ 代码优化 ([8d5e2d3](https://gitee.com/newgateway/vtj/commits/8d5e2d366876cd1c91eb8e4c7b30237e65dd33b5))






## [0.7.34](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.7.33...@vtj/designer@0.7.34) (2024-04-10)


### Bug Fixes

* 🐛 designer about ([0c17f56](https://gitee.com/newgateway/vtj/commits/0c17f56644bbf9f3af974b838917ae5d82881bcd))






## [0.7.33](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.7.32...@vtj/designer@0.7.33) (2024-04-08)


### Bug Fixes

* 🐛 ant-design-vue reset.css path error ([add106d](https://gitee.com/newgateway/vtj/commits/add106dfb13ddb57b3247bbd2683e862c48aecd1))
* 🐛 deps merge ([8c5670b](https://gitee.com/newgateway/vtj/commits/8c5670b2caa4a2c594a7af0e25c0ece9fafb16e6))


### Features

* ✨ 设计器支持ckeditor ([5225869](https://gitee.com/newgateway/vtj/commits/522586927c1ef13ff7e1c02b5df962092805f4a0))






## [0.7.32](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.7.31...@vtj/designer@0.7.32) (2024-04-03)


### Bug Fixes

* 🐛 大纲树default插槽不显示 ([67e7266](https://gitee.com/newgateway/vtj/commits/67e7266c1e92073b95262a1b69caf038a0d00692))






## [0.7.31](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.7.30...@vtj/designer@0.7.31) (2024-04-03)


### chore

* 🚀 格式化提交信息 ([fede392](https://gitee.com/newgateway/vtj/commits/fede3924392a8297d2b2fe37565fd975116b8bf2))


### Features

* ✨ api mock ([df7400f](https://gitee.com/newgateway/vtj/commits/df7400f1c2f7aa20f24e5217b177a38877de5cdd))


### BREAKING CHANGES

* 🧨 no






## [0.7.31](https://gitee.com/newgateway/vtj/compare/@vtj/designer@0.7.30...@vtj/designer@0.7.31) (2024-04-03)


### chore

* 🚀 格式化提交信息 ([fede392](https://gitee.com/newgateway/vtj/commits/fede3924392a8297d2b2fe37565fd975116b8bf2))


### Features

* ✨ api mock ([df7400f](https://gitee.com/newgateway/vtj/commits/df7400f1c2f7aa20f24e5217b177a38877de5cdd))


### BREAKING CHANGES

* 🧨 no
